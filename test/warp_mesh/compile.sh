#!/bin/bash

#
# warpMesh compilation script
#

LIB_DIR=$(readlink -f ../../lib)

FC=gfortran
FFLAGS=$(echo -O3 -Wall -I../../include -L../../lib -Wl,-rpath="$LIB_DIR")

$FC $FFLAGS warpmesh.f90 -o warpmesh.bin -lsolverutils
