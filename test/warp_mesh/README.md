# warpMesh

This is a very simple app to show capabilities of using `solverutilities.so`.
Performs the following operations:
    a. Reads a typical fem MESH. 
    b. Reads a displacement solution.
    c. Modifies the current coordinates from the original mesh (a.) by means
       of warp via the displaments given in the solution (b.)
    d. Writes the new mesh.

## How to compile the app. 

Use the script `compile.sh` for ease the compilation proceduce. 

```bash
./compile.sh
```

## How to test

The script `compile.sh` will generate the app `warpmesh.bin`. This app takes the following
arguments (in order): 
 - solution file: name of the solution file.
 - ipU: integer denoting the position of displacement DOF. 
 - mesh_file: name for the new mesh file. 

```bash 
./warpmesh.bin solution.txt 0 mesh_warp.txt
```

Alternatively, it's possible to use the script `runAll.sh` 
```bash
./runAll.sh
```

## Results. 

### Original Mesh

### New Mesh
