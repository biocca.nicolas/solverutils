program warpmesh

    use LIB_Mesh
    use LIB_Model
    use LIB_DataOut    
    implicit none
    
    type(Mesh)    :: meshA
    type(Model)   :: modelA
    type(dataout) :: sol
    
    character(len=120) :: mesh_file
    character(len=120) :: dataout_file
    character(len=120) :: mesh_file_input
    integer(4)         :: ierr
    
    
    integer(4) :: i,r
    integer(4) :: ipU  ! pointer to displacement DOFs
    integer(4) :: ndim ! mesh dimension
   
    integer(4) :: ipNod, ipDof
    
    ! to use pointer attributes from classes Mesh and dataout must 
    ! be defined as target in order to be 'targeted' by pointers.
    
    real(8), pointer :: Xp(:)
    real(8), pointer :: Solp(:)
    
    integer(4)        :: Narg         ! number of command line arguments
    character(len=80) :: arg          ! command line arg as char
    
    
    ! 
    ! Parse command line arguments    
    !     
    
    Narg = command_argument_count()

    if ( Narg.EQ.4) then
        call get_command_argument(1,dataout_file)
        
        call get_command_argument(2,arg)
        read(arg,'(I10)') ipU                            ! cast operation. string -> int 
    
        call get_command_argument(3,mesh_file_input)
        
        call get_command_argument(4,mesh_file)

    else if (Narg.EQ.1) then
        !write(*,'(A)') '# HELP MESSAGE'
        call get_command_argument(1,arg)
        if ( (trim(Upper_Case(arg)) == '-H') .or. (trim(Upper_Case(arg)) == '--HELP') ) then
            iErr = Help()   ! show help section
            stop
        endif
    else
        write(*,'(A)') 'Inconsistent arguments'
        write(*,'(A)') 'exit'
        stop
    endif
    
    ! 
    ! Init message
    ! 
    
    write(*,'(A)') '# init warpmesh application with the following parameters:'
    write(*,*) 'dataout = ', TRIM(dataout_file)
    write(*,*) 'ipU =', ipU
    write(*,*) 'input mesh = ', TRIM(mesh_file_input)
    write(*,*) 'output mesh = ', TRIM(mesh_file)

    ! 
    ! Read Mesh
    ! 
    
    write(*,'(A)') 'info: reading mesh file'
    meshA%meshfile = TRIM(mesh_file_input)
    ierr = meshA%read_gpsolver_mesh()

    !
    ! Read Model. Just need it to read idoft and nsubsteps
    !
    
    ierr = modelA%get_idofT(meshA%meshfile)
    !ierr = modelA%get_nsubstep()             ! assumes Basparam.txt
    
    ! 
    ! Read DataOut from filename
    ! 
     
    write(*,'(A)') 'info: reading dataout file'
    ierr = sol%alloc(meshA%nodt,modelA%idoft)
    ierr = sol%read_from_file(dataout_file)
    
    ! 
    ! perform mesh warp
    ! 
    write(*,'(A)') 'info: warping mesh coordinates with displacements from DataOut'
    do i=1, meshA%nodt
        ipNod = (i-1)*meshA%ndim
        ipDof = (i-1)*modelA%idoft
        do r=1, meshA%ndim
            meshA%X(ipNod+r) = meshA%X(ipNod+r) + sol%sol(ipDof+ipU+r)
        enddo
    enddo

    !
    ! export mesh 
    ! 
    write(*,'(A)') 'info: exporting mesh'
    ierr = meshA%write_gpsolver_mesh(modelA,mesh_file)


    !
    ! final message
    !
    write(*,'(A)') 'info: operation finished'


CONTAINS

    function Help() result(E_IO)
        !------------------------------------------------------------------
        !
        ! Write minimal manual pages on screen
        !
        !------------------------------------------------------------------
        integer(4) E_IO

        write(*,*) 'warpMesh: warp the coordinates of a given mesh by means'
        write(*,*) 'of a displacement field.'

        write(*,*) '### How to call it'
        write(*,*) '$: warpMesh $dataout_file $ipU $mesh_file_input $mesh_file_output'
        write(*,*) 
        write(*,*) '$dataout_file [String]: gpsolver dataOut File. Usually named DataOutN-%08d.txt'
        write(*,*) '$ipU [integer]: pointer to displacement field in $dataout_file'
        write(*,*) '$mesh_file_input [string]: gpsolver Mesh file'
        write(*,*) '$mesh_file_output [string]: gpsolver Mesh file'
        write(*,*)

    end function 

    FUNCTION Upper_Case(string)
        ! -----
        ! The Upper_Case function converts the lower case characters of a string to upper case one.
        ! Use this function in order to achieve case-insensitive: all character variables used
        ! are pre-processed by Uppper_Case function before these variables are used. So the users
        ! can call functions without pey attention of case of the keywords passed to the functions.
        ! -----
        IMPLICIT NONE
        ! -----
        CHARACTER(LEN=*), INTENT(IN):: string     ! string to be converted
        CHARACTER(LEN=LEN(string))::   Upper_Case ! converted string
        INTEGER::                      n1         ! characters counter
        ! -----

        Upper_Case = string
        DO n1=1,LEN(string)
            SELECT CASE(ICHAR(string(n1:n1)))
                CASE(97:122)
                    Upper_Case(n1:n1)=CHAR(ICHAR(string(n1:n1))-32) ! Upper case conversion
            END SELECT
        ENDDO

        ! -----
    END FUNCTION Upper_Case


  end program
