module LIB_Model
    !use LIB_Mesh, only : Mesh
    implicit none
    private

    type, public :: Model

        character(len=120) :: bpmfile = 'Basparam.txt' ! basparam file

        integer(4) :: nsbtp        ! short for # substeps
        integer(4) :: iWriteElem   ! if writing by elements is on iWriteElem=1 and so nsbtp = nsbtp + 1

        !
        ! the following data structure was taken from MOD_control_parameter developed by dancab.
        ! Nota: Algunas variables fueron comentadas porque dependen de la clase Malla.
        !

        LOGICAL    :: NonLinearProblem    !   Flag that indicates if the problem is nonLinear

        !INTEGER    :: NodT                !   Number of total nodes in the problem
        !INTEGER    :: NelT                !   Number of total elements in the problem
        !INTEGER    :: NDim                !   Dimension of the current problem domain
        INTEGER    :: iDofT               !   Amount of degrees of freedom.
        INTEGER    :: nOldTimeSteps       !   Amount of previous steps used in the solution of current step
        INTEGER    :: MaxIterG            !   Maximum global iterations
        INTEGER    :: nkt                 !   Number of step between writing information to stdout
        INTEGER    :: NStepsRenum         !   Amount of steps before renumbering
        INTEGER    :: NSubSteps           !   Amount of substeps in system solving iterations
        !INTEGER    :: MaxLRows            !   !DOCME
        !INTEGER    :: MaxCoup             !   !DOCME
        !INTEGER    :: iWDataSet           !   !DOCME
        !INTEGER    :: nGoPs               !   !DOCME
        !INTEGER    :: MaxId_DataSet       !   Amount of parameters in the Paramfile
        INTEGER    :: nStepOut            !   Number of step between writing the output (for executionVerbose only)
        INTEGER    :: NodOUT              !   Amount of nodes printed in the output (for executionVerbose only)
        !INTEGER    :: MaxNodEl            !   Maximum number of nodes per element
        INTEGER    :: MaxElemLib          !   Amount of elements in the iElementLib
        INTEGER    :: MaxLCommonPar       !   Maximum number of parameters for each element in the iElementLib in the basparam
        !INTEGER    :: iWPO                !   Flag for write parallel output (allways false in the current implementation)
        !INTEGER    :: NodL                !   Number of nodes in the local processor
        !INTEGER    :: NnlNodes            !   Number of coupling nodes with other processors
        !INTEGER    :: NelL                !   Number of elements in the local processor
        !INTEGER    :: initialTime         !   Initial time of execution from Inifile

        REAL*8     :: SRPG                !   Global subrelaxation parameter
        REAL*8     :: DelT                !   Time step for the execution
        REAL*8     :: Tini                !   Initial time of execution, read from IniFile (in case of a continuation run, it is the value of the last solution)
        REAL*8     :: Tmax                !   Final time of execution
        REAL*8     :: Tini_restart        !   Initial time to restart computations.

        !     Vectors O(1)
        !
        REAL*8,   ALLOCATABLE   :: rNormG(:)        ! valor tipico p/c/DoF (iDoFT)
        REAL*8,   ALLOCATABLE   :: TolG(:)          ! toler. p/convergencia en c/DoF (iDoFT)
        INTEGER,  ALLOCATABLE   :: iDofNamesG(:)    ! Idem (en enteros)
        !INTEGER,  ALLOCATABLE   :: iXupdateG(:)     ! Indicador p/actualizar coordenadas (nDim). Almost deprecated value.

        LOGICAL,  ALLOCATABLE   :: Sym(:)           ! Symmetric linear system ??? (NSubSteps)
        LOGICAL,  ALLOCATABLE   :: Iterative(:)     ! Solve iteratively ??? (NSubSteps)
        INTEGER,  ALLOCATABLE   :: MaxIter(:)       ! Max. NL? iterations ??? (NSubSteps)
        INTEGER,  ALLOCATABLE   :: iSolverType(:)   ! Solver type ??? (NSubSteps)
        REAL*8,   ALLOCATABLE   :: SRP(:)           ! Sub-Relaxation parameter ??? (NSubSteps)
        INTEGER,  ALLOCATABLE   :: ITMAXSOL(:)      ! Max. iterations linear solver??? (NSubSteps)
        REAL*8,   ALLOCATABLE   :: EPS(:)           ! Epsilon parameter for iterative method. Deprecated by PetSc use (NSubSteps)
        INTEGER,  ALLOCATABLE   :: Krylov(:)        ! Krylov parameter for iterative method. Deprecated by PetSc use (NSubSteps)
        INTEGER,  ALLOCATABLE   :: LFIL(:)          ! LFIL parameter for iterative method. Deprecated by PetSc use (NSubSteps)
        REAL*8,   ALLOCATABLE   :: TolPre(:)        ! Tolerance Pre parameter for iterative method. Deprecated by PetSc use (NSubSteps)
        REAL*8,   ALLOCATABLE   :: rNorm(:,:)       ! rNorm parameter for iterative method. (iDofT, NSubSteps)
        INTEGER,  ALLOCATABLE   :: errorType(:)     ! indicates which type of error norm computes. (NSubSteps)
        REAL*8,   ALLOCATABLE   :: Tol(:,:)         ! Convergence tolerance parameter for iterative method.  (iDofT, NSubSteps)
        REAL*8,   ALLOCATABLE   :: CommonPar(:,:)   ! CommonPar for each substep defined in the Basparam file. (MaxElemLib*MaxLCommonPar, NSubSteps)
        INTEGER,  ALLOCATABLE   :: iElementLib(:,:) ! Elements identificators for the current problem. (5*MaxElemLib, NSubSteps)
        INTEGER,  ALLOCATABLE   :: iDofNames (:,:)  ! Names of each degree of freedom in the current problem (iDofT*Lch, NSubSteps))
        INTEGER,  ALLOCATABLE   :: iXupdate (:,:)   ! !DOCME (Ndim, NSubSteps))
        !INTEGER,  ALLOCATABLE   :: MC (:)           ! !DOCME (MaxCoup). (nb note:Esto no puede seguir así!!)
        !INTEGER,  ALLOCATABLE   :: iCoupling (:,:,:)! Degree of freedom coupling matrix (MaxLRows,MaxLRows,MaxCoup). (nb note: Esto no puede seguir así!!)


        INTEGER, ALLOCATABLE    :: NodesOut(:)      ! list of nodes to print to screen.

        LOGICAL :: flag_continuation_run = .FALSE.

        !
        ! Fin MOD_control_parameters
        !

        character(len=30), allocatable :: dofNamesG(:)
        character(len=30), allocatable :: dofNames(:,:)



        ! some data restricted to # of substeps


        integer(4), allocatable :: idir(:)
        real(8), allocatable :: dir(:)
        real(8), allocatable :: sol(:)


    contains ! pointers to methods (procedures)

        procedure :: get_nsubstep => get_nsubstep
        procedure :: get_idofT => get_idofT
        procedure :: read_gpsolver_model => read_gpsolver_model

    end type

    contains

    function read_gpsolver_model(this) result(ierr)
    !----------------------------------------------------------------
    ! Parse model parameters. Almost everything from Basparam file.
    !
    ! Author: Nicolás Biocca.
    !----------------------------------------------------------------
    implicit none
    class(Model), intent(inout) :: this
    integer(4)                 :: ierr

    ! internal variables
    integer(4) :: iounit
    character(len=120) :: Str
    integer(4) :: i

    integer(4) :: LCommonPar  ! LCommonPar = this%MaxElemLib*this%MaxLCommonPar

    !
    ! open Basparam
    !
    iounit = 15
    open(iounit, FILE = TRIM(this%bpmfile), action = 'read')!, READONLY) ! add read-only status for safety purposes
    Str='*TimeStep'

    !
    ! Read Nodal Dof from Basparam. Not implemented yet.
    !

    rewind(iounit)
    Str='*DOF'
    ierr = FindStringInFile(Str,iounit)
    if (ierr.NE.0) then
        write(*,*) Str, "NOT FOUND"
        write(*,'(A)') 'info: Reading Legacy Model'
    else
        read(ioUnit,*) this%iDofT
    endif


    ! empty lines

    !
    ! reading temporal options at *TimeStep flag.
    !
    rewind(iounit)
    Str='*TimeStep'
    ierr = FindStringInFile(Str,iounit)
    if (ierr.NE.0) then
        write(*,*) Str, "NOT FOUND"
        stop
    endif
    READ(ioUnit,*) this%DelT, this%Tini, this%Tmax

    !
    ! optional argument. Restart computations from a given time.
    !

    Str='*continuation_run' ! optional argument
    rewind(iounit)
    ierr = FindStringInFile(Str,iounit)
    if (ierr.EQ.0) then
        write(6,*) '*continuation_run FOUND. Use routine ReadInitialConditions to read.'
        this%flag_continuation_run = .TRUE.
        READ(iounit,*) this%Tini_restart
    endif


    !
    ! output control. Can proceed with default values.
    !

    Str='*OutputControl'
    rewind(ioUnit)
    ierr = FindStringInFile(Str,iounit)
    if (ierr.ne.0) then
        Write(6,*)Str
        Write(6,*)"NOT FOUND, Proceed with default values"
        this%nkt=1 ; this%nStepOut=1 !dancab: timesteps jump for output: to screen, to file
    else
        READ(ioUnit,*) this%nkt,this%nStepOut
    endif

    !
    ! output solution for selected nodes.
    !

    rewind(iounit)
    Str='*NodeOutputControl'
    ierr = FindStringInFile(Str,ioUnit)
    if (ierr.ne.0) then
        Write(6,*)Str
        Write(6,*)"NOT FOUND, Proceed with default value 0"
        this%NodOUT = 0
    else
        READ(iounit,*) this%NodOUT
    End if

    if (this%NodOUT.GT.0) then
        allocate (  this%NodesOut(this%NodOUT) )
        READ(iounit,*) (this%NodesOut(i), i=1,this%NodOUT)
    endif

    !
    ! reading element library control. That is, number of groups per substep and CommonPar length.
    !

    rewind(iounit)
    Str='*ElementLibraryControl'
    ierr = FindStringInFile(Str,iounit)
    if (ierr.NE.0) then
        Write(6,*)Str
        Write(6,*)"NOT FOUND"
        Stop
    End if
    READ(iounit,*) this%MaxElemLib, this%MaxLCommonPar


    !
    ! reading renumbering on/off and number of steps to be done.
    !

    Str='*Renumbering'
    rewind(iounit)
    ierr = FindStringInFile(Str,iounit)
    if (ierr.NE.0) then
        Write(6,*)Str, "NOT FOUND, Proceed with default value 0"
        this%NStepsRenum = 0
    else
        READ(iounit,*) this%NStepsRenum
    endif

    !
    ! Counting number of substeps
    !

    this%NSubSteps=0
    ierr = 0
    rewind(iounit)
    Str='*SubStep'
    do while (ierr.EQ.0)
        ierr = FindStringInFile(Str,iounit)
        if (ierr.EQ.0) this%NSubSteps = this%NSubSteps +1
    enddo
    if (this%NSubSteps.EQ.0) then
        Write(6,*)'NSubSteps = 0, At least ONE Substep must exist'
        stop
    endif

    !
    ! pseudotime. Not implemented yet.
    !

    ! empty lines
    ! empty lines

    !
    ! Str = '*ErrorTypeSubStep'
    !

    !
    ! Str='*ElementalOut'. If writing by elements is activated, then this the treatement
    ! is like an adittional substep.
    !

    rewind(iounit)
    Str='*ElementalOut'
    ierr = FindStringInFile(Str,iounit)
    if (ierr.NE.0) then
        this%iWriteElem = 0
    else
        this%iWriteElem = 1
    endif


    !
    ! Reading element library from Basparam
    !

    ! SOME KIND OF HUGE SECTION
    ! SOME KIND OF HUGE SECTION
    ! SOME KIND OF HUGE SECTION

    ! allocates memory.
    allocate(this%Sym(this%NSubSteps))
    allocate(this%Iterative(this%NSubSteps))
    allocate(this%MaxIter(this%NSubSteps))
    allocate(this%iSolverType(this%NSubSteps))
    allocate(this%SRP(this%NSubSteps))
    allocate(this%ITMAXSOL(this%NSubSteps))
    allocate(this%EPS(this%NSubSteps))
    allocate(this%Krylov(this%NSubSteps))
    allocate(this%LFIL(this%NSubSteps))
    allocate(this%TolPre(this%NSubSteps))

    ! StepTime errors/tolerances
    allocate(this%rNormG(this%iDofT))
    allocate(this%TolG(this%iDofT))
    ! SubSteps errors/tolerances
    allocate(this%rNorm(this%iDofT ,this%NSubSteps))
    allocate(this%Tol  (this%iDofT ,this%NSubSteps))

    ! OK, probably this is not the best way to store this data, but for consistency purposes...
    LCommonPar = this%MaxElemLib * this%MaxLCommonPar
    allocate(this%CommonPar(LCommonPar,this%NSubSteps+this%iWriteElem))
    allocate(this%iElementLib( 5*this%MaxElemLib , this%NSubSteps+this%iWriteElem))

    !
    ! dof names. New structure as characters. forget integers and legacy format.
    ! REMARK: Some trick or external module (e.g. strings) is needed to read this kind of data.
    !

    allocate(this%dofNamesG(this%iDofT))
    allocate(this%dofNames(this%iDofT,this%nSubSteps))

    !
    ! Probably could be a deprecated feature.
    ! At the time of writing,
    !

    !!Allocate (this%iXupdateG(Ndim))
    !!Allocate (this%iXupdate(Ndim,this%NSubSteps))

    !
    ! Now it is time to read
    !

    !
    ! reading Step controls (Global iterations)
    !
    rewind(iounit)
    Str='*StepContinuationControl'
    iErr = FindStringInFile(Str,iounit)
    if (iErr.NE.0) then
        Write(6,*)Str
        Write(6,*)"NOT FOUND"
        Stop
    End if
    read (iounit,*) this%NonLinearProblem,this%MaxIterG,this%SRPG,this%nOldTimeSteps
    read (iounit,*) ( this%rNormG(i)    , i=1 , this%iDofT )
    read (iounit,*) ( this%TolG(i)      , i=1 , this%iDofT )
    read (iounit,*) ( this%DOfNamesG(i) , i=1 , this%iDofT )
    !read (iounit,*) ( this%iXupdateG(N) , N=1 , Ndim ) Esto tal vez se puede bypassear haciendo NDIM3 una constante.


    !
    ! reading library elements
    !
    rewind(iounit)
    Str='*SubStep'
    do i=1, this%nSubSteps

        iErr = FindStringInFile(Str,iounit)

        call ReadLibrary(this%iElementLib(1,i), this%CommonPar(1,i), &
                         this%dofNames(1,i), this%rNorm(1,i), this%Tol(1,i), &
                         this%Sym(i), this%Iterative(i), this%MaxIter(i), &
                         this%iSolverType(i), this%SRP(i), this%ITMAXSOL(i), &
                         this%EPS(i), this%Krylov(i), this%LFIL(i), this%TolPre(i), &
                         this%iDofT, this%MaxElemLib, this%MaxLCommonPar, iounit)

        !
        ! Cambios aquí. No se dimensiona más la cantidad de matrices de acoplamiento
        ! según el id que estas tengan.
        ! Número de matrices de acoplamiento fijo -> nSubSteps*MaxElemLib
        ! Las matrices de acoplamiento son SIEMPRE programadas. En este sentido, no le veo de
        ! gran utilidad la reutilización de las mismas.
        !
        ! IDEA.
        ! Las matrices de acoplamiento debieran ser un tipo derivado/clase.
        ! Estan tendrían como atributo la coupling propiamente dicha Coupling(MaxLRows,MaxLRows)
        ! pero tambien los indices con no zeros (por ejemplo, en formato coordinado por simplicidad)
        ! estos indices se corresponderian con rows y cols no nulas. Luego, en el procesa de
        ! ensamblado numerico se podría interar sobre estos indices, agilizando el procedimiento
        ! y simplificando la rutina de ensamblaje (no mas preguntas del tipo iCoupling(*,*).EQ.0)
        ! La clase deberia tener un metodo para el calculo de no zeros.
        !
        !


        !
        ! En versión legacy, aquí se asignan punteros a las funciones correspondientes de los
        ! elementos. Esta etapa debería pasarse a mainGeneric.
        !


    enddo






    end function

    function get_idofT(this,meshfile) result(ierr)

    implicit none
    class(Model), intent(inout) :: this
    !class(Mesh), intent(in)     :: thisMesh
    character(len=*),intent(in) :: meshfile
    integer(4)                  :: ierr

    ! internal variables
    integer(4) :: iounit
    character(len=120) :: Str


    iounit = 15
    open(iounit, FILE = TRIM(meshfile)) ! add read-only status for safety purposes

    rewind(iounit)
    Str='*Nodal DOFs'
    ierr = FindStringInFile(Str,iounit)
    read(iounit,*) this%iDofT

    close(iounit)

    end function

    function get_nsubstep(this) result(ierr)
    !
    ! Calcula el numero de substeps del modelo.
    !
    !
    !
    implicit none
    class(Model), intent(inout) :: this
    integer(4) :: ierr

    !
    integer(4)         :: nsbtp ! number of substeps
    integer(4)         :: iounit
    character(len=120) :: Str

    iounit = 15
    open(iounit, FILE = TRIM(this%bpmfile)) ! add read-only statement

    rewind(iounit)
    nsbtp = 0
    do while (.TRUE.)
        Str  = '*substep'
        ierr = FindStringInFile(Str,iounit)
        if (ierr.EQ.0) then
            nsbtp = nsbtp + 1
        else
            exit
        endif
    enddo

    if (nsbtp.GT.0) then
        this%nsbtp = nsbtp
        ierr = 0
    else
        ierr = 1
    endif

    close(iounit)

    end function

    SubRoutine ReadLibrary(iElementLib,CommonPar,dofNames,rNorm,Tol, &
      &  Sym,Iterative,MaxIter,iSolverType,SRP,ITMAXSOL,           &
      &  EPS,Krylov,LFIL,TolPre,                                   &
      &  iDofT,MaxElemLib,MaxLCPar,IUnit)
    !----------------------------------------------------------------
    ! Rutina para leer parametros de un substep y alocarlos en las
    ! estructuras de datos correspondientes.
    ! Esta rutina fue tomada de la antigua version de gpsolver.
    ! Algunos cambios se realizaron.
    !   -
    !   -
    !
    ! Author: Nicolás Biocca
    !----------------------------------------------------------------
    implicit none
    ! i/o variables
    integer(4)       :: iElementLib(MaxElemLib,*)
    real(8)          :: CommonPar(MaxLCPar,*)
    character(len=*) :: dofNames(idoft)
    real(8)          :: rNorm(idoft)
    real(8)          :: Tol(idoft)
    !real(8)          :: iXupdate(ndim) ! or (NDIM3) como cte para no necesitar un atributo de la clase malla.
    logical          :: Sym
    logical          :: Iterative
    integer(4)       :: maxIter
    integer(4)       :: iSolverType
    real(8)          :: SRP
    integer(4)       :: ITMAXSOL
    real(8)          :: EPS
    integer(4)       :: Krylov
    integer(4)       :: LFIL
    real(8)          :: TolPre
    integer(4)       :: MaxElemLib, maxLCPar, idofT
    integer(4)       :: IUnit

    ! internal variables
    integer(4) :: N,L,i


    !       Parameter (Lch=16)
!    Character(Lch) Names(iDofT)
!    dimension iNames(Lch*iDofT),iElementLib(MaxElemLib,*),iXupdate(*)
!    Real*8 rNorm(*),Tol(*),CommonPar(MaxLCPar,*)
!    Real*8 SRP,EPS,TolPre
!    Logical Sym,Iterative

    Read (IUnit,*) Sym, Iterative ,MaxIter ,SRP ,iSolverType
    Select CASE (iSolverType)
        Case (-1,0)
        Case (1,2,3)
            backspace(IUnit)
            READ(IUnit,*)Sym,Iterative,MaxIter,SRP,iSolverType,ITMAXSOL,EPS
        Case (4,5,6)
            backspace(IUnit)
            Read (IUnit,*)Sym,Iterative,MaxIter,SRP,iSolverType,ITMAXSOL,EPS,Krylov
        Case (100:)
            backspace(IUnit)
            Read (IUnit,*)Sym,Iterative,MaxIter,SRP,iSolverType,ITMAXSOL,EPS,Krylov,LFIL,TolPre
        CASE Default
            Write (6,*)"Invalid Solver Type"
            Stop
    end select

    Do L=1,MaxElemLib
        Read (IUnit,*) (iElementLib(L,I), I=1 , 5 )
    End do
    Do L=1,MaxElemLib
        Read (IUnit,*) (CommonPar(I,L)  , I=1 ,MaxLCPar)
    End do

    Read (IUnit,*) ( rNorm(N)    , N=1 , iDofT )
    Read (IUnit,*) ( Tol(N)      , N=1 , iDofT )
    Read (IUnit,*) ( dofNames(N) , N=1 , iDofT )
    !Read (IUnit,*) ( iXupdate(N) , N=1 , Ndim )

    END


    FUNCTION FindStringInFile(Str, ioUnit) RESULT (iError)
        ! -----
        ! Busca un String en un archivo (STR), sino lo encuentra rebovina el archivo
        ! y pone iError < 0 como indicador de no hallazgo
        ! Str: String to find, ioUnit: Unit assigned to Input File; iError: Error Status variable
        ! -----
        IMPLICIT NONE
        ! Parameters
        LOGICAL,PARAMETER  :: Segui=.True.
        ! Arguments
        CHARACTER(*), INTENT(IN) :: Str
        INTEGER, INTENT (IN) :: ioUnit
        ! Locals
        CHARACTER(LEN=120) :: DummyString
        CHARACTER(LEN=120) :: upStr
        INTEGER :: iError
        INTEGER :: Leng
        ! -----

        iError=0
        Leng = LEN_TRIM(Str)
        upStr = Upper_Case(Str)       ! Line added by NB. Converts Str to Upper Case string
        ! ---
        !REWIND(ioUnit) By comment this it works for counting # tags in files.
        Search_Loop: DO WHILE (segui)
            READ(ioUnit,'(1A120)',IOSTAT=iError) DummyString ! iError es 0 si lee con exito, >0 si hay error y <0 si es end of file.
            DummyString = Upper_Case(DummyString)   ! line added by NB
            !       if (iError==59) backspace(ioUnit)
            IF (iError.lt.0) THEN
                REWIND (ioUnit)
                EXIT Search_Loop
            END IF
            IF ( DummyString(1:1)    /=    '*'      ) CYCLE Search_Loop
            IF ( DummyString(1:Leng) == upStr(1:Leng) ) EXIT Search_Loop
        END DO Search_Loop

    END FUNCTION FindStringInFile

    FUNCTION Upper_Case(string)
        ! -----
        ! The Upper_Case function converts the lower case characters of a string to upper case one.
        ! Use this function in order to achieve case-insensitive: all character variables used
        ! are pre-processed by Uppper_Case function before these variables are used. So the users
        ! can call functions without pey attention of case of the keywords passed to the functions.
        ! -----
        IMPLICIT NONE
        ! -----
        CHARACTER(LEN=*), INTENT(IN):: string     ! string to be converted
        CHARACTER(LEN=LEN(string))::   Upper_Case ! converted string
        INTEGER::                      n1         ! characters counter
        ! -----

        Upper_Case = string
        DO n1=1,LEN(string)
            SELECT CASE(ICHAR(string(n1:n1)))
                CASE(97:122)
                    Upper_Case(n1:n1)=CHAR(ICHAR(string(n1:n1))-32) ! Upper case conversion
            END SELECT
        ENDDO

        ! -----
    END FUNCTION Upper_Case


end module LIB_Model
