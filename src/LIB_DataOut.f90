module LIB_DataOut

    use LIB_Model, only : Model
    use LIB_Mesh, only : Mesh
    implicit none
    private

    !real(8), allocatable(:) :: dataout

    type, public :: dataout
        real(8), allocatable :: sol(:)
        real(8)              :: time
    contains
        procedure :: alloc => create
        procedure :: read_from_file => read_dataout_from_file
        procedure :: read_from_steptime => read_dataout_from_steptime
    end type dataout

    contains

    function create(this,nodT,idofT) result(ierr)
    !----------------------------------------------------------------
    ! allocates memory.
    !
    ! This is a not very well done OOP approach because nodT and idoft
    ! are passed by argument and not through its own class. The later 
    ! is done for KISS principles purposes.
    !
    !----------------------------------------------------------------
    implicit none
    class(dataout), intent(inout) :: this
    integer(4), intent(in)        :: nodT
    integer(4), intent(in)        :: idofT
    integer(4)                    :: ierr

    allocate(this%sol(nodT*idofT),STAT=ierr)

    end function

    function read_dataout_from_file(this,filename) result(ierr)
    !----------------------------------------------------------------
    ! Read DataOut given its filename
    !
    !
    !----------------------------------------------------------------
    implicit none
    class(dataout), intent(inout) :: this
    character(len=*), intent(in)  :: filename
    integer(4)                    :: ierr

    ! internal variables
    integer(4)        :: iounit
    character(len=80) :: Str
    integer(4)        :: idummy     ! integer dummy
    real(8)           :: rdummy     ! real dummy

    !
    ! check if this%sol has already been allocated
    !
    if (.NOT. ALLOCATED(this%sol)) then
        write(*,*) 'memory not allocated yet'
        ierr = 1
        return
    endif

    !
    ! open file
    !

    iounit = 15
    open(iounit,FILE = TRIM(filename))
    rewind(iounit)

    !
    ! find *Time in file
    !

    Str = '*Time'
    ierr = FindStringInFile(Str, iounit)
    if (ierr.EQ.0) then
        ! read step, time, dtm
        read(iounit,*) idummy, this%time, rdummy

        ! read raw data.
        read(iounit,*) this%sol
    else
        write(*,'(A,X,A)') TRIM(Str), "NOT FOUND"
        ierr = 1
        close(iounit)
        return
    endif

    !
    ! close unit
    !

    close(iounit)

    !
    ! indicates ierr=0 as a success signal if it get here.
    !

    ierr = 0

    end function

    function read_dataout_from_steptime(this,steptime) result(ierr)
    !----------------------------------------------------------------
    ! Lee DataOut dado el step time
    !
    !
    !----------------------------------------------------------------
    implicit none
    class(dataout), intent(inout) :: this
    integer(4),     intent(in)    :: steptime
    integer(4)                    :: ierr

    ! internal variables
    integer(4)        :: iounit
    character(len=80) :: Str
    integer(4)        :: idummy     ! integer dummy
    real(8)           :: rdummy     ! real dummy
    character(len=220):: filename

    !
    ! check if this%sol has already been allocated
    !
    if (.NOT. ALLOCATED(this%sol)) then
        write(*,*) 'memory not allocated yet'
        ierr = 1
        return
    endif


    !
    ! parse steptime to DataOutN-*.txt filename
    !

    write(filename,'(1A9,1I8.8,1A4)') "DataOutN-",steptime,".txt"

    ! Code below this line is the same as "read_dataout_from_file"
    ! Code below this line is the same as "read_dataout_from_file"
    ! Code below this line is the same as "read_dataout_from_file"

    !
    ! open file
    !

    iounit = 15
    open(iounit,FILE = TRIM(filename))
    rewind(iounit)

    !
    ! find *Time in file
    !

    Str = '*Time'
    ierr = FindStringInFile(Str, iounit)
    if (ierr.EQ.0) then
        ! read step, time, dtm
        read(iounit,*) idummy, this%time, rdummy

        ! read raw data.
        read(iounit,*) this%sol
    else
        write(*,'(A,X,A)') TRIM(Str), "NOT FOUND"
        ierr = 1
        close(iounit)
        return
    endif

    !
    ! close unit
    !

    close(iounit)

    !
    ! indicates ierr=0 as a success signal if it get here.
    !

    ierr = 0

    end function

    FUNCTION FindStringInFile(Str, ioUnit) RESULT (iError)
    !----------------------------------------------------------------
    ! Place the cursor after a given string (Str) in an opened file 
    ! by means of its own unit number (ioUnit). If doesn't find returns
    ! iError < 0 as not found indicator and rewind the current file.
    !
    ! Str: String to find
    ! ioUnit: Unit assigned to Input File
    ! iError: Error Status variable
    !----------------------------------------------------------------
    IMPLICIT NONE
    ! Parameters
    LOGICAL,PARAMETER  :: Segui=.True.
    ! Arguments
    CHARACTER(*), INTENT(IN) :: Str
    INTEGER, INTENT (IN) :: ioUnit
    ! Locals
    CHARACTER(LEN=120) :: DummyString
    CHARACTER(LEN=120) :: upStr
    INTEGER :: iError
    INTEGER :: Leng
    ! -----

    iError=0
    Leng = LEN_TRIM(Str)
    upStr = Upper_Case(Str)       ! Line added by NB. Converts Str to Upper Case string
    ! ---
    !REWIND(ioUnit) By comment this it works for counting # tags in files.
    Search_Loop: DO WHILE (segui)
        READ(ioUnit,'(1A120)',IOSTAT=iError) DummyString ! iError es 0 si lee con exito, >0 si hay error y <0 si es end of file.
        DummyString = Upper_Case(DummyString)   ! line added by NB
        !       if (iError==59) backspace(ioUnit)
        IF (iError.lt.0) THEN
            REWIND (ioUnit)
            EXIT Search_Loop
        END IF
        IF ( DummyString(1:1)    /=    '*'      ) CYCLE Search_Loop
        IF ( DummyString(1:Leng) == upStr(1:Leng) ) EXIT Search_Loop
    END DO Search_Loop

    END FUNCTION FindStringInFile

    FUNCTION Upper_Case(string)
        ! -----
        ! The Upper_Case function converts the lower case characters of a string to upper case one.
        ! Use this function in order to achieve case-insensitive: all character variables used
        ! are pre-processed by Uppper_Case function before these variables are used. So the users
        ! can call functions without pey attention of case of the keywords passed to the functions.
        ! -----
        IMPLICIT NONE
        ! -----
        CHARACTER(LEN=*), INTENT(IN):: string     ! string to be converted
        CHARACTER(LEN=LEN(string))::   Upper_Case ! converted string
        INTEGER::                      n1         ! characters counter
        ! -----

        Upper_Case = string
        DO n1=1,LEN(string)
            SELECT CASE(ICHAR(string(n1:n1)))
                CASE(97:122)
                    Upper_Case(n1:n1)=CHAR(ICHAR(string(n1:n1))-32) ! Upper case conversion
            END SELECT
        ENDDO

        ! -----
    END FUNCTION Upper_Case

end module LIB_DataOut
