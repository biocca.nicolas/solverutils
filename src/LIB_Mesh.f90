module LIB_Mesh
    use LIB_Model, only : Model
    implicit none
    private

    type, public ::  Mesh

        integer(4) :: format   ! format. From format 2 onwards physical groups info is embeed on Mesh.txt

        integer(4) :: nodt     ! number of nodes
        integer(4) :: nelt     ! number of elements
        integer(4) :: ndim     ! dimension number


        real(8), allocatable    :: X(:)        ! mesh coordinates
        integer(4), allocatable :: IE(:)       ! incidence
        integer(4), allocatable :: JE(:)
        integer(4), allocatable :: IET(:)      ! incidence transpose
        integer(4), allocatable :: JET(:)

!   physical groups for the mesh.
        integer(4) :: nGroups
        integer(4), allocatable    :: Nodel(:)
        integer(4), allocatable    :: Neltg(:)
        character(20), allocatable :: NelName(:)
        character(80), allocatable :: PhysicalName(:)


        integer(4), allocatable :: ElementType(:)
        integer(4), allocatable :: ElementMat(:)
        integer(4), allocatable :: idGroup(:)
        integer(4), allocatable :: FirstG(:)

        !
        ! Material properties (from Param.txt)
        !
        integer(4) :: MaxId_DataSet                 !Amount of parameters in the Paramfile
        integer(4), allocatable :: Ie__Param(:)     ! pointer to Param
        integer(4), allocatable :: Ie_JParam(:)     ! pinter to Jparam
        real(8), allocatable    :: Param(:)         ! real Param data
        integer(4), allocatable :: JParam(:)        ! integer Param data


        !
        ! Mesh.txt and Param.txt files
        !

        character(len=120) :: meshfile = 'Mesh.txt'
        character(len=120) :: paramfile = 'Param.txt'

    contains

        procedure :: read_gpsolver_mesh => read_gpsolver_mesh
        procedure :: write_gpsolver_mesh => write_gpsolver_mesh
        procedure :: read_gpsolver_param => read_gpsolver_param
        procedure :: write_gpsolver_param => write_gpsolver_param

    end type Mesh


    contains

    function write_gpsolver_param(this) result(ierr)
    !
    ! method to write material properties (Param,JParam) to paramfile
    !
    implicit none
    class(Mesh), intent(in) :: this
    integer(4)              :: ierr

    ! internal variables
    character(len=80) :: Str
    integer(4) :: iounit                        ! i/o unit
    integer(4) :: I,J,N,K,L                     ! some counters

    integer(4) :: Length_Param
    integer(4) :: Length_JParam

    iounit = 15 ! stop doing this please

    open(iounit,FILE = TRIM(ADJUSTL(this%paramfile)))
    rewind(iounit)

    !
    ! writting header. Parameters groups corresponding to material groups.
    !

    write(iounit,'(1A17)')'*Parameter Groups'
    write(iounit,*) this%MaxId_DataSet

    !
    ! writting real parameters (Param)
    !

    write(iounit,'(1A16)')'*Real Parameters'
    write(iounit,'(10I8)') (this%Ie__Param(I+1)-this%Ie__Param(I),I=1,this%MaxId_DataSet)

    Length_Param = this%Ie__Param(this%MaxId_DataSet+1)-1
    if (Length_Param.NE.0) then
        write(iounit,'(3E14.6)') (this%Param(k),k=1,Length_Param)
    endif

    !
    ! writting integer parameters (JParam)
    !

    write(iounit,'(1A19)')'*Integer Parameters'
    write(iounit,'(10I8)') (this%Ie_JParam(I+1)-this%Ie_JParam(I),I=1,this%MaxId_DataSet)

    Length_JParam = this%Ie_JParam(this%MaxId_DataSet+1)-1
    if (Length_JParam.NE.0) then
        write(iounit,'(10I8)') (this%JParam(k),k=1,Length_JParam)
    endif

    !
    ! stop writting. Close unit.
    !

    close(iounit)

    !
    ! indicates ierr=0 as a success signal if it get here.
    !

    ierr = 0

    end function

    function read_gpsolver_param(this) result(ierr)
    !
    ! read material parameters (Param and JParam) -> (real, integer)
    !
    implicit none
    class(Mesh), intent(inout) :: this
    integer(4)                 :: ierr

    ! internal variables
    character(len=80) :: Str
    integer(4) :: iounit                        ! i/o unit
    integer(4) :: I,J,N,K,L                     ! some counters

    integer(4) :: Length_Param
    integer(4) :: Length_JParam

    iounit = 15 ! stop doing this please

    open(iounit,FILE = TRIM(ADJUSTL(this%paramfile)), STATUS= 'OLD')  ! read only
    rewind(iounit)

    Str = '*Parameter Groups'
    ierr = FindStringInFile(Str, iounit)
    if (ierr.NE.0) then
        write(*,'(A,X,A)') TRIM(Str), "NOT FOUND"
        stop

        !
        ! Here, instead of stop execution an error code (through ierr) could be implemented
        !

        ierr = -1
        return
    endif
    read(iounit,*) this%MaxId_DataSet

    !
    ! Allocate pointers
    !

    allocate(this%Ie__Param(this%MaxId_DataSet+1))
    allocate(this%Ie_JParam(this%MaxId_DataSet+1))

    !
    ! Read them securely. To do so, handle with the special case MaxId_DataSet = 0
    !
    if (this%MaxId_DataSet.GT.0) then

        !
        ! Reading real parameters
        !

        rewind(iounit)
        Str='*Real Parameters'
        ierr = FindStringInFile(Str, iounit)
        if (ierr.NE.0) then
            write(*,'(A,X,A)') TRIM(Str), "NOT FOUND"
            stop
        endif
        read(iounit,*) (this%Ie__Param(I) ,I=1, this%MaxId_DataSet)

        Length_Param  = IEarrange(this%Ie__Param,this%MaxId_DataSet)

        !
        ! allocates memory. Two times for time dependent params.
        ! If that is correct, for consitency purposes should be done with
        ! nOldTimeSteps to take into account high order temporal schemes.
        !

        allocate(this%Param(MAX(1,2*Length_Param)))
        allocate(this%JParam(MAX(1,2*Length_Param)))

        if (Length_Param.NE.0) then
            read(iounit,*) (this%Param(I),I=1, Length_Param)
            do i=1,Length_Param
                this%Param(i+Length_Param)=this%Param(i)
            enddo
        endif

        !
        ! Reading integer parameters
        !

        rewind(ioUnit)
        Str='*Integer Parameters'
        ierr = FindStringInFile(Str,ioUnit)
        if (ierr.NE.0) then
            write(*,'(A,X,A)') TRIM(Str), "NOT FOUND"
            stop
        endif
        read(ioUnit,*) (this%Ie_JParam(I) ,I=1, this%MaxId_DataSet)
        Length_JParam  = IEarrange(this%Ie_JParam,this%MaxId_DataSet)
        if (Length_JParam.NE.0) then
            read(iounit,*) (this%JParam(I),I=1, Length_JParam)
            do i=1,Length_JParam
                this%JParam(i+Length_JParam)=this%JParam(i)
            enddo
        endif

    else
        this%Ie__Param=1
        this%Ie_JParam=1
        allocate(this%Param (1))
        allocate(this%JParam(1))
    endif

    !
    ! stop reading. Dirichlet conditions are deprecated.
    !

    close(iounit)

    !
    ! indicates ierr=0 as a success signal if it get here.
    !

    ierr = 0

    end function

    function read_gpsolver_mesh(this) result(ierr)
    !
    !
    ! The following flags (case-insensitive) are read:
    !   *meshformat
    !   *dim
    !   *coordinates
    !   *ELEMENT GROUPS
    !   *ELEMENT T(YPE)
    !   *ELEMENT M(ATERIAL)
    !
    !
    implicit none
    class(Mesh), intent(inout) :: this
    integer(4)                 :: ierr

    character(len=80) :: Str

    integer(4) :: iounit                        ! i/o unit
    integer(4) :: I,J,N,K,L                     ! some counters
    integer(4) :: nGroupsL,NelTL,ng,NelP,LenN,Nel,LengthJE
    integer(4) :: NodesAnt,nDimL,NodTL,iDoftL

    iounit = 15
    open(iounit,FILE = TRIM(ADJUSTL(this%meshfile)))
    rewind(iounit)

    Str = '*meshformat'
    ierr = FindStringInFile(Str, iounit)
    if (ierr.EQ.0) then
        read(iounit,*) this%format
    else
        write(*,'(A,X,A)') TRIM(Str), "NOT FOUND"
        write(*,'(A)') 'SET LEGACY FORMAT'
        this%format = 1
    endif



    Str = '*dim'
    rewind(iounit)
    ierr = FindStringInFile(Str, iounit)
    if (ierr.EQ.0) then
        read(iounit,*) this%ndim
    else
        write(*,'(A,X,A)') TRIM(Str), "NOT FOUND"
        stop
        return
    endif

    !
    ! read mesh coordinates. 1st look for nodT, allocatates and the
    ! reads xll.
    !

    rewind(iounit)
    Str = '*COORDINATES'
    ierr = FindStringInFile(Str, iounit)
    if (ierr.NE.0) then
        write(*,*) Str, " NOT FOUND"
        stop
    endif
    read(iounit,*) this%nodt

    allocate(this%x(this%ndim*this%nodt))
    do I=1, this%nodt
        read(iounit,*) (this%x(this%ndim*(I-1) + J ),J=1,this%ndim)
    enddo

    !
    ! reading element groups.
    !
    rewind(iounit)
    Str = '*ELEMENT GROUPS'
    ierr = FindStringInFile(Str, iounit)
    if (ierr.NE.0) then
        write(*,*) Str, "NOT FOUND"
        stop
    endif
    read(iounit,*) this%ngroups
    nGroupsL = this%ngroups
    allocate(this%NelTg(nGroupsL),this%NodEl(nGroupsL),this%NelName(nGroupsL))
    allocate(this%FirstG(nGroupsL))

    do i=1,nGroupsL
        read(iounit,*) N, this%NelTg(i),this%NelName(i)
    enddo

    this%nelt = SUM(this%NelTg)
    NelTL = this%nelt
    allocate(this%ie(NelTL+1))
    allocate(this%idGroup(NelTL))
    this%ie=0

    !be aware, naive implementation. Only GENERIC groups are considered (and just one).
    ! Se asume que si hay elementos genericos, hay un solo grupo
    N = 0 ! Count the groups with generic type
    do i=1,nGroupsL
        LenN = Len_Trim(this%NelName(i))
        If ( Upper_Case(this%NelName(i)(1:LenN)) == "GENERIC") then
            N = N + 1
        endif
    enddo

    ! safety stage from SAU's implementation.
    if ( N .ge. 1 .and. this%nGroups .gt. 1 )then
        write (6,*)"More than one group with one with the Generic type"
        write (6,*)"Using Generic type group Only ONE GROUP is allowed"
        Stop
    endif

    if (N.EQ.1) then
        do i=1, NelTL
            read(iounit,*) this%ie(i)
        enddo
        this%idGroup   = 1
        this%NelTg(1)  = NelTL
        this%FirstG(1) = 0
    endif

    !
    ! reading incidence JE
    !

    LengthJE = SUM(this%ie)
    allocate(this%je(LengthJE))
    rewind(iounit)
    Str = '*INCIDENCE'
    ierr = FindStringInFile(Str, iounit)
    if (ierr.NE.0) then
        write(*,*) Str, "NOT FOUND"
        stop
    endif
    read(iounit,*) this%je


    !
    ! reading elementType
    !

    rewind(iounit)
    Str = '*ELEMENT T'  ! stands for element type
    ierr = FindStringInFile(Str, iounit)
    if (ierr.EQ.0) then
        allocate(this%ElementType(NelTL))
        read(iounit,*) this%ElementType
    else
        write(*,'(A,X,A)') TRIM(Str), "NOT FOUND"
        stop
    endif

    !
    !
    !

    allocate(this%ElementMat(NelTL))
    rewind(iounit)
    Str='*ELEMENT M' ! stands for element material
    ierr = FindStringInFile(Str, iounit)
    if (ierr.EQ.0) then
        read(iounit,*) this%ElementMat
    else
        write(*,*) Str, "NOT FOUND"
        write(*,*) 'assumes that all element have they own material property.'
        this%ElementMat = (/(i, i=1, NelTL)/)
    endif


    !
    ! read Physical groups. The number of physical groups can be read from
    ! this%ElementType(NelTL), last element or compute the max value from
    ! the same array.
    !

    if (this%format.GE.2) then

        rewind(iounit)
        Str='*PHYSICAL'
        ierr = FindStringInFile(Str, iounit)
        if (ierr.EQ.0) then
            this%nGroups = this%ElementType(NelTL)
            allocate(this%Nodel(this%nGroups))
            allocate(this%Neltg(this%nGroups))
            allocate(this%NelName(this%nGroups))
            allocate(this%PhysicalName(this%nGroups))

            do i=1, this%nGroups
                read(iounit,*) this%PhysicalName(i), this%Neltg(i), this%NelName(i), this%Nodel(i)
            enddo
         else
            write(*,'(A)') 'info: *physical group not found. Mandatory field for mesh format above 2.'
            stop
         endif
    endif


    !
    ! stop reading. Dirichlet conditions are deprecated.
    !

    close(iounit)

    !
    ! transform IE to a pointer of JE. It use to be done by IEArrange function.
    !

    NodesAnt=this%ie(1)
    this%ie(1)=1
    Do Nel=2,this%NelT+1
        N           = this%ie(Nel)
        this%ie(Nel) = this%ie(Nel-1) + NodesAnt
        NodesAnt    = N
    Enddo

    !
    ! indicates ierr=0 as a success signal if it get here.
    !

    ierr = 0


    end function

    function write_gpsolver_mesh(this,thismodel,meshfile) result(ierr)
    !----------------------------------------------------------------
    ! imprime aspectos geometricos y conectividad de una malla.
    ! Considera condiciones de borde no asignadas.
    !
    !----------------------------------------------------------------
    implicit none
    class(Mesh), intent(in), target        :: this         ! mesh type
    class(Model), intent(in)               :: thismodel    ! model type
    character(len=*), intent(in), optional :: meshfile     ! filename to export
    integer(4)                             :: ierr

    ! internal variables
    integer(4) :: formatL
    integer(4) :: nDimL,NodTL,NelTL,iDoftL,nGroupsL
    integer(4) :: nsbtpL           ! # substeps
    integer(4) :: I,J,N,NFR
    integer(4) :: iounit
    character(len=120) :: Str

    character(len=120) :: mshfile
    character(len=30)  :: fmt

    integer(4) :: k

    real(8), pointer :: xCoor(:)
    integer(4), pointer :: Nodel(:)
    integer(4), pointer :: NelTg(:)
    integer(4), pointer :: JE(:)
    integer(4), pointer :: IE(:)
    integer(4), pointer :: ElementType(:)

    integer(4) :: ipxCoor
    integer(4) :: ipJE1
    integer(4) :: ipJE11

    integer(4) :: iNodel

    !
    ! make derived type variables as internal ones for an easy use
    !

    formatL  = this%format
    NelTL    = this%nelt
    NodTL    = this%nodt
    nDimL    = this%nDim
    nGroupsL = this%nGroups

    iDofTL = thismodel%iDofT
    nsbtpL = thismodel%nsbtp


    ! pointer to data structures located in derived type.
    xCoor => this%X
    Nodel => this%Nodel
    NelTg => this%NelTg
    JE    => this%JE
    IE    => this%IE
    ElementType => this%ElementType


    !
    ! handle optional arguments
    !

    if (PRESENT(meshfile)) then
        mshfile = TRIM(meshfile)  ! use filename passed by argument
    else
        mshfile = this%meshfile   ! use filename from mesh object
    endif


    iounit = 15
    open(iounit,FILE = TRIM(mshfile))

    !
    ! Work in Progress.
    !

    Str = "*MESHFORMAT"
    WRITE(iounit,'(1A30)') Str
    WRITE(iounit,'(1I10)') formatL

    Str = "*PHYSICAL GROUPS"
    WRITE(iounit,'(1A30)') Str
    DO i=1, this%nGroups
        WRITE(iounit,'(1A20,1X,1I10,1X,1A10,1X,1I3)') this%PhysicalName(i), this%Neltg(i), this%NelName(i), this%Nodel(i)
    ENDDO

    ! LEGACY FORMAT
    if (formatL.EQ.1) then
        Str = "*NODAL DOFs"
        WRITE(iounit,'(1A30)') Str
        WRITE(iounit,'(1I10)') iDofTL
    endif

    Str = "*DIMEN"
    WRITE(iounit,'(1A30)') Str
    WRITE(iounit,'(1I10)') nDimL
    WRITE(iounit,*)


    !
    ! writting coordinates
    !

    Str = "*COORDINATES"
    WRITE(iounit,'(1A30)') Str
    WRITE(iounit,'(1I10)') NodTL
    WRITE(iounit,*)
    DO i=1,NodTL
        ipxCoor = (i-1)*nDimL
        WRITE(iounit,'(SP3E17.8E3)') xCoor(ipxCoor+1:ipxCoor+nDimL)
        !if nDim<3 then it will write only two values, no problem with the format specifier which holds space for 3 values
    END DO
    WRITE(iounit,*)

    !
    ! writting element groups.
    !

    Str = "*ELEMENT GROUPS"
    WRITE(iounit,'(1A30)') Str
    WRITE(iounit,'(1I10)') 1
    WRITE(iounit,'(2I10,1A10)') 1, NelTL, "Generic"
    WRITE(iounit,*)

    do i=1, NelTL
        iNodel = IE(i+1) - IE(i)   ! # nodes for i-th element
        write(iounit,'(1I10)') iNodel

        ! if follows a new group . nested if for memory safety.
        if (i.LT.NeltL) then
            if (ElementType(i).NE.ElementType(i+1)) then
                write(iounit,*) ! separator
            endif
        endif
    enddo
    write(iounit,*) ! separator

    !
    ! writting conectivity (incidence)
    !

    Str = "*INCIDENCE"
    write(iounit,'(1A30)') Str
    write(iounit,*)
    do i=1, NeltL
        iNodel = IE(i+1) - IE(i)   ! # nodes for i-th element
        ipJE1 = IE(i)
        write(fmt,*) "(",iNodel,"I10)"    ! output format
        write(iounit,fmt) (JE(ipJE1+k-1), k=1, iNodel)

        ! if follows a new group . nested if for memory safety.
        if (i.LT.NeltL) then
            if (ElementType(i).NE.ElementType(i+1)) then
                write(iounit,*) ! separator
            endif
        endif
    enddo
    write(iounit,*) ! separator

    !
    ! writting element types (element groups in basparam)
    !

    Str = "*ELEMENT TYPE"
    write(iounit,'(1A30)') Str
    do i=1, NeltL
        write(iounit,'(1I10)') ElementType(i)

        ! if follows a new group . nested if for memory safety.
        if (i.LT.NeltL) then
            if (ElementType(i).NE.ElementType(i+1)) then
                write(iounit,*) ! separator
            endif
        endif
    enddo
    write(iounit,*) ! separator

    !
    ! writting element materials (element material in param, each element is a material)
    !

    Str = "*ELEMENT MAT"
    write(iounit,'(1A30)') Str
    do i=1, NeltL
        write(iounit,'(1I10)') i

        ! if follows a new group . nested if for memory safety.
        if (i.LT.NeltL) then
            if (ElementType(i).NE.ElementType(i+1)) then
                write(iounit,*) ! separator
            endif
        endif
    enddo
    write(iounit,*) ! separator

    !
    ! LEGACY FORMAT
    ! writting dirichlet conditions. Assumes null (not specified) overall dofs
    !

    IF (formatL.EQ.1) THEN

        Str = "*DIRICHLET CONDITIONS"
        WRITE(iounit,'(1A30)') Str
        !first the mask for all the substeps
        !format specifier
        WRITE(fmt,*) "(",iDofTL,"I10)"
        do j=1, nsbtpL
            do i=1, NodTL
                write(iounit,fmt) (/(0, k=1, iDofTL)/)
            enddo
            write(iounit,*)
        enddo
        !second the values for all the substeps
        !format specifier
        write(fmt,*) "(",iDofTL,"E17.8E3)"
        do j=1, nsbtpl
            do i=1, NodTL
                write(iounit,fmt) (/(0.0d0, k=1, iDofTL)/)
            enddo
        enddo

    ENDIF

    !
    ! stop writting.
    !

    close(iounit)


    !
    ! if everything is OK, returns ierr = 0
    !

    ierr = 0

    end function

    FUNCTION FindStringInFile(Str, ioUnit) RESULT (iError)
        ! -----
        ! Busca un String en un archivo (STR), sino lo encuentra rebovina el archivo
        ! y pone iError < 0 como indicador de no hallazgo
        ! Str: String to find, ioUnit: Unit assigned to Input File; iError: Error Status variable
        ! -----
        IMPLICIT NONE
        ! Parameters
        LOGICAL,PARAMETER  :: Segui=.True.
        ! Arguments
        CHARACTER(*), INTENT(IN) :: Str
        INTEGER, INTENT (IN) :: ioUnit
        ! Locals
        CHARACTER(LEN=120) :: DummyString
        CHARACTER(LEN=120) :: upStr
        INTEGER :: iError
        INTEGER :: Leng
        ! -----

        iError=0
        Leng = LEN_TRIM(Str)
        upStr = Upper_Case(Str)       ! Line added by NB. Converts Str to Upper Case string
        ! ---
        REWIND(ioUnit)
        Search_Loop: DO WHILE (segui)
            READ(ioUnit,'(1A120)',IOSTAT=iError) DummyString ! iError es 0 si lee con exito, >0 si hay error y <0 si es end of file.
            DummyString = Upper_Case(DummyString)   ! line added by NB
            !       if (iError==59) backspace(ioUnit)
            IF (iError.lt.0) THEN
                REWIND (ioUnit)
                EXIT Search_Loop
            END IF
            IF ( DummyString(1:1)    /=    '*'      ) CYCLE Search_Loop
            IF ( DummyString(1:Leng) == upStr(1:Leng) ) EXIT Search_Loop
        END DO Search_Loop

    END FUNCTION FindStringInFile

    FUNCTION Upper_Case(string)
        ! -----
        ! The Upper_Case function converts the lower case characters of a string to upper case one.
        ! Use this function in order to achieve case-insensitive: all character variables used
        ! are pre-processed by Uppper_Case function before these variables are used. So the users
        ! can call functions without pey attention of case of the keywords passed to the functions.
        ! -----
        IMPLICIT NONE
        ! -----
        CHARACTER(LEN=*), INTENT(IN):: string     ! string to be converted
        CHARACTER(LEN=LEN(string))::   Upper_Case ! converted string
        INTEGER::                      n1         ! characters counter
        ! -----

        Upper_Case = string
        DO n1=1,LEN(string)
            SELECT CASE(ICHAR(string(n1:n1)))
                CASE(97:122)
                    Upper_Case(n1:n1)=CHAR(ICHAR(string(n1:n1))-32) ! Upper case conversion
            END SELECT
        ENDDO

        ! -----
    END FUNCTION Upper_Case


    function IEarrange(IE,NelT) result(length)
    !
    ! rewritten old fashioned function.
    ! Reordena vector IE leido de Mesh o similar y lo reordena para
    ! para apuntar a vector JE.
    !
    !
    implicit none
    integer(4), intent(inout) :: IE(*)
    integer(4), intent(in)    :: NelT
    integer(4)                :: length

    !internal variables
    integer(4) :: LIE
    integer(4) :: LP
    integer(4) :: LP1
    integer(4) :: Nel

    LIE  = 0
    LP   = IE(1)
    IE(1)= 1
    do Nel=2,NelT+1
        LP1     = IE(Nel)
        IE(Nel) = IE(Nel-1) + LP
        LIE     = LIE       + LP
        LP      = LP1
    enddo
    length = LIE

    end function


end module LIB_Mesh
