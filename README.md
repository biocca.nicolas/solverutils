# solverUtils

Library with modules with classes definitions for mesh reading/storing purposes, 
computational modeling parameters, and solution writers. The main goal is to speed
up the develpment of applications based on a reliable source of code.

## Compilation

### Method 1

Compile the library via the `build.sh` script. 
```bash
./build.sh
```

### Method 2

Using the `makefile` file in `build` directory.
```bash
cd build/
make all
```

## Structure

After compilation, the library is distributed in the following manner:
  - lib: `libsolverutils.so` is the library itself. 
  - include: `lib_model.mod`, `lib_mesh.mod`, `lib_dataout.mod`. Interfaces for each modules in 
             `libsolverutils.so` (needed for compilation purposes).


