#!/bin/bash

#
# script for building solverUtils library
#

FC=gfortran
LIB_SO_NAME=libsolverutils.so

SRC_DIR=src
BUILD_DIR=build

SRCS=$PWD/$SRC_DIR/LIB_*.f90

# build needed directories
mkdir $BUILD_DIR lib include

# link sources into build directory
ln -sf $SRCS $BUILD_DIR

# compile and link 
cd $BUILD_DIR > /dev/null                                                      # override print on screen
#echo $FC $FCFLAGS LIB_Model.f90 LIB_Mesh.f90 LIB_DataOut.f90 -o libsolverutils.so   # compilation and linking instruction
$FC -O3 -Wall -fPIC -shared LIB_Model.f90 LIB_Mesh.f90 LIB_DataOut.f90 -o libsolverutils.so 

# distribute files accordingly. 
cp $LIB_SO_NAME ../lib
cp *.mod ../include


